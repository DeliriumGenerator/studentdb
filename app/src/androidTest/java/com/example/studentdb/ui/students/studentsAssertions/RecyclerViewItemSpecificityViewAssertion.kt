package com.example.studentdb.ui.students.studentsAssertions

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.NoMatchingViewException

import org.hamcrest.StringDescription
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.ViewAssertion
import org.hamcrest.Matcher
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat


class RecyclerViewItemSpecificityViewAssertion(specificallyId: Int, matcher: Matcher<View?>) : ViewAssertion  {
    private val mSpecificallyId = specificallyId
    private val mMatcher = matcher

    override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
        val description = StringDescription()
        description.appendText("'")
        mMatcher.describeTo(description)
        val itemRoot = view as ViewGroup
        val typeIUsage: View = itemRoot.findViewById(mSpecificallyId)
        if (noViewFoundException != null) {
            description.appendText(
                String.format(
                    "' check could not be performed because view with id '%s' was not found.\n",
                    mSpecificallyId
                )
            )
            Log.e("RecyclerViewItemSpecificityView", description.toString())
            throw noViewFoundException
        } else {
            description.appendText("' doesn't match the selected view.")
            assertThat(description.toString(), typeIUsage, mMatcher)
        }
    }
}