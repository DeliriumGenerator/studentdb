package com.example.studentdb.ui.students


import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.example.studentdb.MainActivity
import com.example.studentdb.R
import com.example.studentdb.ui.students.customViewActions.*
import com.example.studentdb.ui.students.studentsAssertions.*
import org.junit.*
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class WorksTest {

    @get:Rule
    var mActivityRule = ActivityScenarioRule(
        MainActivity::class.java
    )

    @Before
    @Throws(Exception::class)
    fun addStud(){
        onView(withId(R.id.add_student)).perform(click())
        onView(withId(R.id.button_save)).perform(click())
        onView(withId(R.id.students)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    clickChildViewWithId(R.id.popup)
                )
            )
        onView(withText("Работы")).perform(click())
    }

    @After
    @Throws(Exception::class)
    fun deleteStud(){
        onView(withId(R.id.back)).perform(click())
        onView(withId(R.id.students)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    clickChildViewWithId(R.id.popup)
                )
            )
        onView(withText("Удалить")).perform(click())
    }

    @Test
    fun `Add work test`(){
        onView(withId(R.id.add_work)).perform(click())
        onView(withId(R.id.button_save)).perform(click())
        onView(withId(R.id.works)).check(RecyclerViewItemCountAssertion(1))
    }

    @Test
    fun `Edit work test`() {
        onView(withId(R.id.add_work)).perform(click())
        onView(withId(R.id.button_save)).perform(click())
        onView(withId(R.id.works)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    clickChildViewWithId(R.id.card_view)
                )
            )
        onView(withText("Редактировать")).perform(click())
        onView(withId(R.id.edit_text_theme)).perform(replaceText("Море"))
        onView(withId(R.id.button_save)).perform(click())

        onView(withId(R.id.works)).check(RecyclerViewItemCountAssertion(1))
        onView(withId(R.id.works)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
            .check(RecyclerViewItemSpecificityViewAssertion(R.id.theme, withText("Море")))
    }

    @Test
    fun `Delete work test`(){
        onView(withId(R.id.add_work)).perform(click())
        onView(withId(R.id.button_save)).perform(click())
        onView(withId(R.id.works)).check(RecyclerViewItemCountAssertion(1))
        onView(withId(R.id.works)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    clickChildViewWithId(R.id.card_view)
                )
            )
        onView(withText("Удалить")).perform(click())
        onView(withId(R.id.works)).check(RecyclerViewItemCountAssertion(0))
    }
}
