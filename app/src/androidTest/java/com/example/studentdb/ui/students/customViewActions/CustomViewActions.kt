package com.example.studentdb.ui.students.customViewActions

import android.view.MotionEvent
import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import org.hamcrest.Matcher

fun clickChildViewWithId(id: Int): ViewAction {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View>? {
            return null
        }

        override fun getDescription(): String {
            return ""
        }

        override fun perform(
            uiController: UiController,
            view: View
        ) {
            val clickView = view.findViewById<View>(id)
            clickView.performClick()
        }
    }
}