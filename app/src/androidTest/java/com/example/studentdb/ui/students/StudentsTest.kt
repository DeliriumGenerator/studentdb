package com.example.studentdb.ui.students


import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.example.studentdb.MainActivity
import com.example.studentdb.R
import com.example.studentdb.ui.students.customViewActions.*
import com.example.studentdb.ui.students.studentsAssertions.*
import org.junit.*
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class StudentsTest {

    @get:Rule
    var mActivityRule = ActivityScenarioRule(
        MainActivity::class.java
    )

    @Before
    @Throws(Exception::class)
    fun setUp() {
    }

    private fun addStud(){
        onView(withId(R.id.add_student)).perform(click())
        onView(withId(R.id.button_save)).perform(click())
    }

    private fun deleteStud(position: Int = 0){
        onView(withId(R.id.students)).perform(scrollToPosition<RecyclerView.ViewHolder>(position))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    position,
                    clickChildViewWithId(R.id.popup)
                )
            )
        onView(withText("Удалить")).perform(click())
    }

    @Test
    fun `Add student test`(){
        onView(withId(R.id.add_student)).perform(click())
        onView(withId(R.id.edit_text_name)).perform(replaceText("Фамилия Имя"))
        onView(withId(R.id.edit_text_phone)).perform(replaceText("--1234567890"))
        onView(withId(R.id.edit_text_birth_date)).perform(replaceText("23052010"))
        onView(withId(R.id.edit_text_balance)).perform(replaceText("100"))
        onView(withId(R.id.edit_text_notes)).perform(replaceText("Заметка"))
        onView(withId(R.id.button_save)).perform(click())
        onView(withId(R.id.students)).check(RecyclerViewItemCountAssertion(1))
        onView(withId(R.id.students)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
            .check(RecyclerViewItemSpecificityViewAssertion(R.id.name, withText("Фамилия Имя")))
            .check(RecyclerViewItemSpecificityViewAssertion(R.id.phone, withText("+7 (123) 456-78-90")))
            .check(RecyclerViewItemSpecificityViewAssertion(R.id.age, withText("12 лет")))
            .check(RecyclerViewItemSpecificityViewAssertion(R.id.balance, withText("На счету: 100 руб.")))

        deleteStud()
    }

    @Test
    fun `Edit student test`() {
        addStud()
        onView(withId(R.id.students)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    clickChildViewWithId(R.id.popup)
                )
            )
        onView(withText("Редактировать")).perform(click())
        onView(withId(R.id.edit_text_name)).perform(replaceText("Имя Фамилия"))
        onView(withId(R.id.edit_text_phone)).perform(replaceText("+7 (098) 765-43-21"))
        onView(withId(R.id.edit_text_birth_date)).perform(replaceText("23052015"))
        onView(withId(R.id.edit_text_balance)).perform(replaceText("200"))
        onView(withId(R.id.edit_text_notes)).perform(replaceText(""))
        onView(withId(R.id.button_save)).perform(click())
        onView(withId(R.id.students)).check(RecyclerViewItemCountAssertion(1))
        onView(withId(R.id.students)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
            .check(RecyclerViewItemSpecificityViewAssertion(R.id.name, withText("Имя Фамилия")))
            .check(RecyclerViewItemSpecificityViewAssertion(R.id.phone, withText("+7 (098) 765-43-21")))
            .check(RecyclerViewItemSpecificityViewAssertion(R.id.age, withText("7 лет")))
            .check(RecyclerViewItemSpecificityViewAssertion(R.id.balance, withText("На счету: 200 руб.")))
        deleteStud()
    }

    @Test
    fun `Delete student test`(){
        addStud()
        onView(withId(R.id.students)).perform(scrollToPosition<RecyclerView.ViewHolder>(1))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    clickChildViewWithId(R.id.popup)
                )
            )
        onView(withText("Удалить")).perform(click())
        onView(withId(R.id.students)).check(RecyclerViewItemCountAssertion(0))
    }

    //TODO("Доделать взаимодействие с range bar")
    @Test
    fun `Filter students test`(){
        onView(withId(R.id.add_student)).perform(click())
        onView(withId(R.id.button_save)).perform(click())

        onView(withId(R.id.add_student)).perform(click())
        onView(withId(R.id.edit_text_balance)).perform(replaceText("100"))
        onView(withId(R.id.edit_text_birth_date)).perform(replaceText("23052013"))
        onView(withId(R.id.button_save)).perform(click())

        onView(withId(R.id.add_student)).perform(click())
        onView(withId(R.id.edit_text_balance)).perform(replaceText("200"))
        onView(withId(R.id.edit_text_birth_date)).perform(replaceText("23052014"))
        onView(withId(R.id.button_save)).perform(click())

        onView(withId(R.id.add_student)).perform(click())
        onView(withId(R.id.edit_text_balance)).perform(replaceText("-300"))
        onView(withId(R.id.edit_text_birth_date)).perform(replaceText("23052015"))
        onView(withId(R.id.button_save)).perform(click())

        onView(withId(R.id.add_student)).perform(click())
        onView(withId(R.id.edit_text_balance)).perform(replaceText("-400"))
        onView(withId(R.id.edit_text_birth_date)).perform(replaceText("23052016"))
        onView(withId(R.id.button_save)).perform(click())

        onView(withId(R.id.add_student)).perform(click())
        onView(withId(R.id.edit_text_balance)).perform(replaceText("500"))
        onView(withId(R.id.edit_text_birth_date)).perform(replaceText("23052017"))
        onView(withId(R.id.button_save)).perform(click())

        onView(withId(R.id.filter)).perform(click())
        onView(withId(R.id.switch_age)).perform(click())
        onView(withId(R.id.button_save)).perform(click())

        onView(withId(R.id.students)).check(RecyclerViewItemCountAssertion(5))

        onView(withId(R.id.filter)).perform(click())
        onView(withId(R.id.age_rangebar)).perform(click())
        onView(withId(R.id.button_save)).perform(click())

        onView(withId(R.id.students)).check(RecyclerViewItemCountAssertion(1))

        onView(withId(R.id.filter)).perform(click())
        onView(withId(R.id.switch_age)).perform(click())
        onView(withId(R.id.button_save)).perform(click())

        repeat(6){
            deleteStud()
        }
    }
}
