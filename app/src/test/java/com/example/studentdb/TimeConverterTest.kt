package com.example.studentdb

import com.example.studentdb.database.methods.TimeConverter.*
import org.junit.Assert.*
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class TimeConverterTest {
    @Test
    fun `toSeconds function test`(){
        assertEquals(0, "01.01.1970".toSeconds(),)
        assertEquals(-2208988800, "01.01.1900".toSeconds())
        assertEquals(832809600, "23.05.1996".toSeconds())
        assertEquals(1684713600, "22.05.2023".toSeconds())
    }

    @Test
    fun `toStringDate function test`(){
        assertEquals("01.01.1970", 0L.toStringDate())
        assertEquals("01.01.1900", (-2208988800L).toStringDate())
        assertEquals("23.05.1996", 832809600L.toStringDate())
        assertEquals("22.05.2023", 1684713600L.toStringDate())
    }

    @Test
    fun `getAge function test`(){
        val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
        assertEquals(0, LocalDate.now().format(formatter).getAge())
        assertEquals(1, "28.02.2000".getAge(LocalDate.parse("28.02.2001", formatter)))
        assertEquals(1, "28.02.2000".getAge(LocalDate.parse("01.03.2001", formatter)))
        assertEquals(0, "28.02.2000".getAge(LocalDate.parse("27.02.2001", formatter)))
        assertEquals(1, "28.02.1999".getAge(LocalDate.parse("28.02.2000", formatter)))
        assertEquals(1, "28.02.1999".getAge(LocalDate.parse("01.03.2000", formatter)))
        assertEquals(0, "28.02.1999".getAge(LocalDate.parse("27.02.2000", formatter)))
    }

    @Test
    fun `isDateFormat function test`(){
        assert("23.05.1996".isDateFormat())
        assert("01.01.1900".isDateFormat())
        assert("01.01.1970".isDateFormat())
        assert("22.05.2023".isDateFormat())
        assert(!"43.05.1996".isDateFormat())
        assert(!"01.1.1900".isDateFormat())
        assert(!"01101.1970".isDateFormat())
        assert(!"".isDateFormat())
    }
}