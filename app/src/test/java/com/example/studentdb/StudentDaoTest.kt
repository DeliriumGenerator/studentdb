package com.example.studentdb

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.studentdb.database.AppDatabase
import com.example.studentdb.database.methods.StudentDao
import com.example.studentdb.database.tables.Student
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


@RunWith(RobolectricTestRunner::class)
class StudentDaoTest {
    private var db: AppDatabase? = null
    private var studentDao: StudentDao? = null
    private val scope = CoroutineScope(Job())
    private lateinit var latch: CountDownLatch
    private lateinit var dbStudents: List<Student>
    private var size: Int? = null
    private val stud = Student(
        1,
        "",
        0,
        "",
        0,
        ""
    )

    @Before
    @Throws(Exception::class)
    fun createDb() {
        db = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().targetContext,
            AppDatabase::class.java
        ).build()
        studentDao = db!!.studentDao()
        latch = CountDownLatch(1)
        dbStudents = listOf()
    }

    @After
    @Throws(Exception::class)
    fun closeDb() {
        db!!.close()
    }

    @Test
    @Throws(java.lang.Exception::class)
    fun `Student DAO insert test`() {
        scope.launch{
            studentDao?.insert(stud)
            dbStudents = studentDao?.getStudents() ?: listOf()
            size = studentDao?.getLastId()
            latch.countDown()
        }
        latch.await(1, TimeUnit.SECONDS)
        assertEquals(1, dbStudents.size)
        assertEquals(1, size)
        assertEquals(stud, dbStudents[0])
    }

    @Test
    @Throws(java.lang.Exception::class)
    fun `Student DAO update test`() {
        scope.launch{
            studentDao?.insert(stud)
            latch.countDown()
        }
        latch.await(1, TimeUnit.SECONDS)
        latch = CountDownLatch(1)
        scope.launch{
            studentDao?.update(stud.copy(name = "Kirill"))
            dbStudents = studentDao?.getStudents() ?: listOf()
            latch.countDown()
        }
        latch.await(1, TimeUnit.SECONDS)
        assertEquals(1, dbStudents.size)
        assertEquals("Kirill", dbStudents[0].name)
    }


    @Test
    @Throws(java.lang.Exception::class)
    fun `Student DAO delete test`() {
        scope.launch{
            studentDao?.insert(stud)
            latch.countDown()
        }
        latch.await(1, TimeUnit.SECONDS)
        latch = CountDownLatch(1)
        scope.launch{
            studentDao?.delete(stud)
            dbStudents = studentDao?.getStudents() ?: listOf()
            size = studentDao?.getLastId()
            latch.countDown()
        }
        latch.await(1, TimeUnit.SECONDS)
        assertEquals(null, size)
        assertEquals(0, dbStudents.size)
    }
}