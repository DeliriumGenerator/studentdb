package com.example.studentdb

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.studentdb.database.AppDatabase
import com.example.studentdb.database.methods.StudentDao
import com.example.studentdb.database.methods.WorkDao
import com.example.studentdb.database.tables.Student
import com.example.studentdb.database.tables.Work
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


@RunWith(RobolectricTestRunner::class)
class WorkDaoTest {
    private var db: AppDatabase? = null
    private var studentDao: StudentDao? = null
    private var workDao: WorkDao? = null
    private val scope = CoroutineScope(Job())
    private lateinit var latch: CountDownLatch
    private lateinit var dbWorks: List<Work>
    private var size: Int? = null
    private val stud = Student(
        1,
        "",
        0,
        "",
        0,
        ""
    )
    private val work = Work(
        1,
        1,
        "",
        "",
    )

    @Before
    @Throws(Exception::class)
    fun createDb() {
        db = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().targetContext,
            AppDatabase::class.java
        ).build()
        studentDao = db!!.studentDao()
        workDao = db!!.workDao()
        latch = CountDownLatch(1)
        dbWorks = listOf()
    }

    @After
    @Throws(Exception::class)
    fun closeDb() {
        db!!.close()
    }

    @Test
    @Throws(java.lang.Exception::class)
    fun `Work DAO insert test`() {
        scope.launch{
            studentDao?.insert(stud)
            workDao?.insert(work)
            workDao?.insert(work.copy(workId = 2))
            dbWorks = workDao?.getByStudentId(1) ?: listOf()
            size = workDao?.getLastId()
            latch.countDown()
        }
        latch.await(1, TimeUnit.SECONDS)
        assertEquals(2, dbWorks.size)
        assertEquals(2, size)
        assertEquals(work, dbWorks[0])
    }

    @Test
    @Throws(java.lang.Exception::class)
    fun `Work DAO update test`() {
        scope.launch{
            studentDao?.insert(stud)
            workDao?.insert(work)
            workDao?.update(work.copy(theme = "Sea"))
            dbWorks = workDao?.getByStudentId(1) ?: listOf()
            latch.countDown()
        }
        latch.await(1, TimeUnit.SECONDS)
        assertEquals(1, dbWorks.size)
        assertEquals("Sea", dbWorks[0].theme)
    }

    @Test
    @Throws(java.lang.Exception::class)
    fun `Work DAO delete test`() {
        scope.launch{
            studentDao?.insert(stud)
            workDao?.insert(work)
            workDao?.delete(work)
            dbWorks = workDao?.getByStudentId(1) ?: listOf()
            size = workDao?.getLastId() ?: 0
            latch.countDown()
        }
        latch.await(1, TimeUnit.SECONDS)
        assertEquals(0, size)
        assertEquals(0, dbWorks.size)
    }

    @Test
    @Throws(java.lang.Exception::class)
    fun `Work DAO cascade test`() {
        scope.launch{
            studentDao?.insert(stud)
            workDao?.insert(work)
            studentDao?.delete(stud)
            dbWorks = workDao?.getByStudentId(1) ?: listOf()
            latch.countDown()
        }
        latch.await(1, TimeUnit.SECONDS)
        assertEquals(0, dbWorks.size)
    }
}