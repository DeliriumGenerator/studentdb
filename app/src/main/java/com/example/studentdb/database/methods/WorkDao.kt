package com.example.studentdb.database.methods

import androidx.room.*
import com.example.studentdb.database.tables.Work

@Dao
interface WorkDao {
    @Query("SELECT * FROM Work WHERE studentId = :id")
    suspend fun getByStudentId(id: Int): MutableList<Work>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(work: Work): Long

    @Transaction
    suspend fun upsert(work: Work) {
        val id = insert(work)
        if (id == -1L) {
            update(work)
        }
    }

    @Update
    suspend fun update(work: Work)

    @Delete
    suspend fun delete(student: Work?)

    @Query("SELECT workId FROM Work ORDER BY workId DESC LIMIT 1")
    suspend fun getLastId(): Int?
}