package com.example.studentdb.database.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.example.studentdb.R
import com.example.studentdb.database.tables.Work

class WorkAdapter(
    var works: MutableList<Work>,
    val callback: WorkAdapter.Callback
) : RecyclerView.Adapter<WorkAdapter.WorkViewHolder>() {

    inner class WorkViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var work: Work
        val photo: ImageView = itemView.findViewById(R.id.photo)
        val theme: TextView = itemView.findViewById(R.id.theme)

        @SuppressLint("SetTextI18n")
        fun bind(holder: WorkAdapter.WorkViewHolder, position: Int){
            work = works[position]
            theme.text = work.theme

            photo.setImageURI(work.photo.toUri())
            if(photo.drawable == null){
                photo.setImageResource(R.drawable.sea)
            }

            itemView.setOnClickListener { _ ->
                if (layoutPosition != RecyclerView.NO_POSITION){
                    callback.onClicked(holder, work)
                }}
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.work_layout,
            parent,
            false
        )
        return WorkViewHolder(itemView)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun getItem(position: Int): Work {
        return works[position]
    }

    override fun getItemCount() = works.size

    override fun onBindViewHolder(holder: WorkViewHolder, position: Int) {
        holder.bind(holder, position)
    }

    interface Callback{
        fun onClicked(holder: WorkAdapter.WorkViewHolder, work: Work)
    }
}