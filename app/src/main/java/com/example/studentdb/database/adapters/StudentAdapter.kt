package com.example.studentdb.database.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.studentdb.R
import com.example.studentdb.database.tables.Student
import com.example.studentdb.database.methods.TimeConverter.*
import java.text.SimpleDateFormat


class StudentAdapter(
    var students: MutableList<Student>,
    val callback: Callback
) : RecyclerView.Adapter<StudentAdapter.StudentViewHolder>() {


    inner class StudentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var stud: Student
        val name: TextView = itemView.findViewById(R.id.name)
        val phone: TextView = itemView.findViewById(R.id.phone)
        val balance: TextView = itemView.findViewById(R.id.balance)
        val age: TextView = itemView.findViewById(R.id.age)
        val menu: androidx.appcompat.widget.AppCompatImageButton = itemView.findViewById(R.id.popup)

        @SuppressLint("SetTextI18n")
        fun bind(holder: StudentViewHolder, position: Int){
            stud = students[position]
            this.name.text = students[position].name
            this.phone.text = students[position].phone
            this.balance.text = "На счету: " + students[position].balance.toString() + " руб."

            val age = students[position].birthDate?.toStringDate()?.getAge()
            this.age.text = when {
                age == null -> "Возраст не задан"
                age in (11..14) -> "$age лет"
                age % 10 == 1 -> "$age год"
                age % 10 in 2..4 -> "$age года"
                else -> "$age лет"
            }

            menu.setOnClickListener{ _ ->
                if (layoutPosition != RecyclerView.NO_POSITION){
                    callback.onMenuClicked(holder, stud)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.student_layout,
            parent,
            false
        )
        return StudentViewHolder(itemView)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun getItem(position: Int): Student {
        return students[position]
    }

    override fun getItemCount() = students.size

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        holder.bind(holder, position)
    }

    interface Callback{
        fun onMenuClicked(holder: StudentViewHolder, stud: Student)
    }
}