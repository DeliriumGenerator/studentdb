package com.example.studentdb.database.tables

import android.os.Parcelable
import androidx.room.*
import kotlinx.parcelize.Parcelize

@Entity(indices = [Index("studentId")])
@Parcelize
data class Student(
    @PrimaryKey(autoGenerate = true)
    val studentId: Int,
    val name: String,
    val birthDate: Long?,
    val phone: String,
    val balance: Int,
    val notes: String
): Parcelable
