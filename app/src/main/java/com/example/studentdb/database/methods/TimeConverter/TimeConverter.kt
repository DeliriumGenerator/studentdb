package com.example.studentdb.database.methods.TimeConverter

import java.time.*
import java.time.LocalDateTime.ofEpochSecond
import java.time.format.DateTimeFormatter

private val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")

fun String.isDateFormat(): Boolean {
    return this.length == 10 &&
            this.getOrNull(0) in ('0'..'3') &&
            this.getOrNull(1) in ('0'..'9') &&
            this.getOrNull(2) == '.' &&
            this.getOrNull(3) in ('0'..'9') &&
            this.getOrNull(4) in ('0'..'9') &&
            this.getOrNull(5) == '.' &&
            this.getOrNull(6) in ('0'..'9') &&
            this.getOrNull(7) in ('0'..'9') &&
            this.getOrNull(8) in ('0'..'9') &&
            this.getOrNull(9) in ('0'..'9')
}

fun String.getAge(date: LocalDate = LocalDate.now()): Int {
    return Period.between(
        LocalDate.parse(this, formatter),
        date
    ).years
}

fun String.toSeconds(): Long {
    return LocalDate.parse(this, formatter).atStartOfDay(ZoneOffset.UTC).toEpochSecond()
}


fun Long.toStringDate(): String {
    return Instant.ofEpochSecond(this).atZone(ZoneOffset.UTC).toLocalDate()
        .format(formatter)
}


