package com.example.studentdb.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.studentdb.database.methods.StudentDao
import com.example.studentdb.database.methods.WorkDao
import com.example.studentdb.database.tables.Student
import com.example.studentdb.database.tables.Work


//@Database(entities = [Student::class, Teacher::class, Work::class], version = 1)
@Database(entities = [Student::class, Work::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun studentDao(): StudentDao?
    abstract fun workDao(): WorkDao?
}