package com.example.studentdb.database

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.studentdb.App
import com.example.studentdb.database.methods.TimeConverter.*
import com.example.studentdb.database.tables.Student
import com.example.studentdb.database.tables.Work
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class AppStateViewModel @Inject constructor() : ViewModel(){
    private var studentDao = App.getAppInstance()?.getDatabaseInstance()?.studentDao()!!
    private var workDao = App.getAppInstance()?.getDatabaseInstance()?.workDao()!!
    var balanceRange = MutableLiveData<Pair<Int, Int>>()
    var studentsLiveData = MutableLiveData<MutableList<Student>>()
    var worksLiveData = MutableLiveData<MutableList<Work>>()
    var ageFilterRange = Int.MIN_VALUE to Int.MAX_VALUE
    var balanceFilterRange = Int.MIN_VALUE to Int.MAX_VALUE

    fun getStudents() {
        viewModelScope.launch {
            studentsLiveData.value = withContext(Dispatchers.IO) {
                studentDao.getStudents()
                    .filter {
                        //Ученик подходит по балансу.
                        it.balance >= balanceFilterRange.first &&
                                it.balance <= balanceFilterRange.second &&
                                //Если фильтр выключен показать учеников с неуказанным возрастом.
                                ((it.birthDate == null && ageFilterRange == Int.MIN_VALUE to Int.MAX_VALUE) ||
                                        //В противном случае показать только тех, чей возраст в диапазоне.
                                        (it.birthDate != null && (it.birthDate.toStringDate().getAge() >= ageFilterRange.first &&
                                                it.birthDate.toStringDate().getAge() <= ageFilterRange.second)))
                    }.toMutableList()
            }!!
        }
    }

    fun addStudent(name: String, birthDate: Long?, phone: String, balance: Int, notes: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                studentDao.insert(
                    Student(
                        studentId = (studentDao.getLastId() ?: -1) + 1,
                        name,
                        birthDate,
                        phone,
                        balance,
                        notes
                    )
                )
            }!!
            getStudents()
        }
    }

    fun editStudent(stud: Student) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                studentDao.update(stud)
            }!!
            getStudents()
        }
    }

    fun deleteStudent(stud: Student) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                studentDao.delete(stud)
            }!!
            getStudents()
        }
    }

    fun getWorks(studentId: Int) {
        viewModelScope.launch {
            worksLiveData.value = withContext(Dispatchers.IO) {
                workDao.getByStudentId(studentId)
            }!!
        }
    }

    fun addWork(studentId: Int, theme: String, source: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                workDao.insert(
                    Work(
                        workId = (workDao.getLastId() ?: -1) + 1,
                        studentId,
                        theme,
                        source
                    )
                )
            }!!
            getWorks(studentId)
        }
    }

    fun editWork(work: Work) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                workDao.update(work)
            }!!
            getWorks(work.studentId)
        }
    }

    fun deleteWork(work: Work) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                workDao.delete(work)
            }!!
            getWorks(work.studentId)
        }
    }

    fun getBalanceRange() {
        viewModelScope.launch {
            balanceRange.value = withContext(Dispatchers.IO) {
                val data = studentDao.getStudents()
                (data.minByOrNull { it.balance }?.balance
                    ?: Int.MIN_VALUE) to (data.maxByOrNull { it.balance }?.balance ?: Int.MAX_VALUE)
            }!!
        }
    }

    fun setAgeLimits(pair: Pair<Int, Int>) {
        ageFilterRange = pair
    }

    fun setBalanceLimits(pair: Pair<Int, Int>) {
        balanceFilterRange = pair
    }

}



