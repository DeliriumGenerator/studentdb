package com.example.studentdb.database.tables

import android.os.Parcelable
import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import kotlinx.parcelize.Parcelize

@Entity(indices = [Index("workId")],
    foreignKeys = [ForeignKey(
        entity = Student::class,
        parentColumns = arrayOf("studentId"),
        childColumns = arrayOf("studentId"),
        onDelete = CASCADE,
        onUpdate = CASCADE
    )]
)
@Parcelize
data class Work(
    @PrimaryKey(autoGenerate = true)
    val workId: Int,
    val studentId: Int,
    val theme: String,
    val photo: String
) : Parcelable
