package com.example.studentdb.database.methods

import androidx.room.*
import com.example.studentdb.database.tables.Student
import java.util.*

@Dao
interface StudentDao {
    @Query("SELECT * FROM Student")
    suspend fun getStudents(): MutableList<Student>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(student: Student?): Long

    @Transaction
    suspend fun upsert(student: Student?) {
        val id = insert(student)
        if (id == -1L) {
            update(student)
        }
    }

    @Update
    suspend fun update(student: Student?)

    @Delete
    suspend fun delete(student: Student?)

    @Query("SELECT studentId FROM Student ORDER BY studentId DESC LIMIT 1")
    suspend fun getLastId(): Int?
}