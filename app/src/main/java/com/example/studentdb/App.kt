package com.example.studentdb

import android.app.Application
import androidx.room.Room
import com.example.studentdb.database.AppDatabase
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {
    var database: AppDatabase? = null


    override fun onCreate() {
        super.onCreate()
        instance = this
        database = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database"
        ).build()
    }

    fun getDatabaseInstance(): AppDatabase? {
        return this.database
    }


    companion object {
        var instance: App? = null

        fun getAppInstance(): App? {
            return instance
        }
    }
}