package com.example.studentdb.ui.students.works

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.studentdb.R
import com.example.studentdb.database.AppStateViewModel
import com.example.studentdb.database.tables.Student
import com.example.studentdb.databinding.FragmentAddWorkBinding

class AddWorkFragment : Fragment() {
    private val appStateViewModel: AppStateViewModel by activityViewModels()
    private var _binding: FragmentAddWorkBinding? = null
    private val binding get() = _binding!!
    private lateinit var student: Student

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddWorkBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args: WorksFragmentArgs by navArgs()
        student = args.owner

        binding.buttonAddPhoto.setOnClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main)
                .navigate(AddWorkFragmentDirections.actionToTakePhoto())
        }

        val navController = requireActivity().findNavController(R.id.nav_host_fragment_activity_main)
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<String>("uri")
            ?.observe(viewLifecycleOwner) {
                binding.editTextSource.setText(it)
            }

        binding.buttonSave.setOnClickListener {
            val theme = binding.editTextTheme.text.toString()
            val source = binding.editTextSource.text.toString()
            appStateViewModel.addWork(
                student.studentId,
                theme,
                source
            )

            requireActivity().findNavController(R.id.nav_host_fragment_activity_main).popBackStack()
            appStateViewModel.getWorks(student.studentId)
        }

        binding.buttonCancel.setOnClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main).popBackStack()
        }
    }
}