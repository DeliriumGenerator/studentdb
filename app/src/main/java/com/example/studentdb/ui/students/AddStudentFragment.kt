package com.example.studentdb.ui.students

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.studentdb.R
import com.example.studentdb.database.AppStateViewModel
import com.example.studentdb.database.methods.TimeConverter.toSeconds
import com.example.studentdb.databinding.FragmentAddStudentBinding
import ru.tinkoff.decoro.MaskImpl
import ru.tinkoff.decoro.slots.PredefinedSlots
import ru.tinkoff.decoro.slots.Slot
import ru.tinkoff.decoro.watchers.MaskFormatWatcher


class AddStudentFragment : Fragment() {
    private val appStateViewModel: AppStateViewModel by activityViewModels()
    private var _binding: FragmentAddStudentBinding? = null
    private val binding get() = _binding!!

    private val dateMask = arrayOf(
        PredefinedSlots.digit(),
        PredefinedSlots.digit(),
        PredefinedSlots.hardcodedSlot('.').withTags(Slot.TAG_DECORATION),
        PredefinedSlots.digit(),
        PredefinedSlots.digit(),
        PredefinedSlots.hardcodedSlot('.').withTags(Slot.TAG_DECORATION),
        PredefinedSlots.digit(),
        PredefinedSlots.digit(),
        PredefinedSlots.digit(),
        PredefinedSlots.digit()
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddStudentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        MaskFormatWatcher(MaskImpl.createTerminated(PredefinedSlots.RUS_PHONE_NUMBER))
            .installOn(binding.editTextPhone)
        MaskFormatWatcher(MaskImpl.createTerminated(dateMask)).installOn(binding.editTextBirthDate)

        binding.buttonSave.setOnClickListener {
            val name = binding.editTextName.text.toString()
            val phone = binding.editTextPhone.text.toString()
            val balance = binding.editTextBalance.text.toString().toIntOrNull() ?: 0
            val notes = binding.editTextNotes.text.toString()
            var birthDate: Long? = null
            try {
                birthDate = binding.editTextBirthDate.text.toString().toSeconds()
            } catch (e: Exception) {}

            appStateViewModel.addStudent(
                name,
                birthDate,
                phone,
                balance,
                notes
            )

            requireActivity().findNavController(R.id.nav_host_fragment_activity_main).popBackStack()
        }

        binding.buttonCancel.setOnClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main).popBackStack()
        }
    }
}