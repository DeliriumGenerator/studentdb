package com.example.studentdb.ui.students

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.studentdb.R
import com.example.studentdb.database.AppStateViewModel
import com.example.studentdb.database.adapters.StudentAdapter
import com.example.studentdb.database.tables.Student
import com.example.studentdb.databinding.FragmentStudentsBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class StudentsFragment : Fragment() {

    private var _binding: FragmentStudentsBinding? = null
    private val binding get() = _binding!!

    private lateinit var recycler: RecyclerView
    private lateinit var studentAdapter: StudentAdapter
    private val appStateViewModel: AppStateViewModel by activityViewModels()

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)

        _binding = FragmentStudentsBinding.inflate(inflater, container, false)
        val fragmentView = binding.root
        recycler = binding.students
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(requireActivity().applicationContext)
        studentAdapter = StudentAdapter(mutableListOf(),
            object : StudentAdapter.Callback {
                override fun onMenuClicked(
                    holder: StudentAdapter.StudentViewHolder,
                    stud: Student
                ) {
                    val popup = PopupMenu(holder.itemView.context, holder.menu)
                    popup.menuInflater.inflate(R.menu.students_popup_menu, popup.menu)
                    popup.setOnMenuItemClickListener { menuItem ->
                        when (menuItem.itemId) {
                            R.id.works -> {
                                requireActivity().findNavController(R.id.nav_host_fragment_activity_main)
                                    .navigate(StudentsFragmentDirections.actionToWorks(stud))
                            }
                            R.id.edit -> {
                                requireActivity().findNavController(R.id.nav_host_fragment_activity_main)
                                    .navigate(StudentsFragmentDirections.actionToEdit(stud))
                            }
                            R.id.delete -> {
                                appStateViewModel.deleteStudent(holder.stud)
                            }
                        }
                        true
                    }
                    popup.show()

                }
            })
        recycler.adapter = studentAdapter

        appStateViewModel.studentsLiveData.observe(viewLifecycleOwner) { data ->
            studentAdapter.students = data
            studentAdapter.notifyDataSetChanged()
        }
        appStateViewModel.getStudents()
        return fragmentView
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.students_menu, menu)

        val addButton = menu.findItem(R.id.add_student)
        addButton.setOnMenuItemClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main)
                .navigate(StudentsFragmentDirections.actionToAdd())
            true
        }

        val filterButton = menu.findItem(R.id.filter)
        filterButton.setOnMenuItemClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main)
                .navigate(StudentsFragmentDirections.actionToFilter())
            true
        }

        super.onCreateOptionsMenu(menu, inflater)
    }
}