package com.example.studentdb.ui.students

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.studentdb.R
import com.example.studentdb.databinding.FragmentFilterBinding
import com.example.studentdb.database.AppStateViewModel
import kotlin.math.min
import kotlin.math.max

class FilterFragment : Fragment() {
    private val appStateViewModel: AppStateViewModel by activityViewModels()
    private var _binding: FragmentFilterBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFilterBinding.inflate(inflater, container, false)
        FragmentFilterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val balanceRangebar = binding.balanceRangebar
        val ageRangebar = binding.ageRangebar

        appStateViewModel.balanceRange.observe(viewLifecycleOwner) { data ->
            if (data.first == Int.MIN_VALUE ||
                data.second == Int.MAX_VALUE ||
                data.first == data.second
            ) {
                balanceRangebar.isEnabled = false
                binding.switchBalance.isChecked = false
                binding.switchBalance.setOnCheckedChangeListener { _, _ ->
                    binding.switchBalance.isChecked = false
                }
            } else {
                balanceRangebar.tickStart = data.first.toFloat()
                balanceRangebar.tickEnd = data.second.toFloat()

                balanceRangebar.setRangePinsByValue(
                    max(
                        appStateViewModel.balanceFilterRange.first.toFloat(),
                        balanceRangebar.tickStart
                    ),
                    min(
                        appStateViewModel.balanceFilterRange.second.toFloat(),
                        balanceRangebar.tickEnd
                    )
                )

                binding.switchBalance.isChecked = appStateViewModel.balanceFilterRange != Int.MIN_VALUE to Int.MAX_VALUE
                balanceRangebar.isEnabled = binding.switchBalance.isChecked

                binding.switchBalance.setOnCheckedChangeListener { _, isChecked ->
                    balanceRangebar.isEnabled = isChecked
                }
            }
        }

        appStateViewModel.getBalanceRange()

        try {
            binding.switchAge.isChecked = true
            ageRangebar.setRangePinsByValue(
                appStateViewModel.ageFilterRange.first.toFloat(),
                appStateViewModel.ageFilterRange.second.toFloat()
            )
        } catch (e: Exception) {
            ageRangebar.setRangePinsByValue(0.0F, 18.0F)
            ageRangebar.isEnabled = false
            binding.switchAge.isChecked = false
        }

        binding.switchAge.setOnCheckedChangeListener { _, isChecked ->
            ageRangebar.isEnabled = isChecked
        }

        binding.buttonCancel.setOnClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main).popBackStack()
        }

        binding.buttonSave.setOnClickListener {
            if(balanceRangebar.isEnabled){
                appStateViewModel.setBalanceLimits(balanceRangebar.leftPinValue.toInt() to balanceRangebar.rightPinValue.toInt())
            } else{
                appStateViewModel.setBalanceLimits(Int.MIN_VALUE to Int.MAX_VALUE)
            }
            if(ageRangebar.isEnabled){
                appStateViewModel.setAgeLimits(ageRangebar.leftIndex to ageRangebar.rightIndex)
            } else{
                appStateViewModel.setAgeLimits(Int.MIN_VALUE to Int.MAX_VALUE)
            }
            appStateViewModel.getStudents()
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main).popBackStack()
        }
    }
}