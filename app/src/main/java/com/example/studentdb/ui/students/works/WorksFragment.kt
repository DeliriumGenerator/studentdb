package com.example.studentdb.ui.students.works

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.studentdb.R
import com.example.studentdb.database.AppStateViewModel
import com.example.studentdb.database.adapters.WorkAdapter
import com.example.studentdb.database.tables.Student
import com.example.studentdb.database.tables.Work
import com.example.studentdb.databinding.FragmentWorksBinding

class WorksFragment : Fragment() {

    private var _binding: FragmentWorksBinding? = null
    private val binding get() = _binding!!

    private lateinit var recycler: RecyclerView
    private lateinit var workAdapter: WorkAdapter
    private lateinit var student: Student
    private val appStateViewModel: AppStateViewModel by activityViewModels()

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)

        _binding = FragmentWorksBinding.inflate(inflater, container, false)
        val fragmentView = binding.root
        recycler = binding.works
        recycler.setHasFixedSize(true)
        recycler.layoutManager = GridLayoutManager(requireActivity().applicationContext, 3)

        workAdapter = WorkAdapter(mutableListOf(),
            object : WorkAdapter.Callback {
                override fun onClicked(holder: WorkAdapter.WorkViewHolder, work: Work) {
                    val popup = PopupMenu(holder.itemView.context, holder.photo)
                    popup.menuInflater.inflate(R.menu.work_popup_menu, popup.menu)
                    popup.setOnMenuItemClickListener { menuItem ->
                        when (menuItem.itemId) {
                            R.id.edit -> {
                                requireActivity().findNavController(R.id.nav_host_fragment_activity_main)
                                    .navigate(WorksFragmentDirections.actionToEditWork(work))
                            }
                            R.id.delete -> {
                                appStateViewModel.deleteWork(work)
                            }
                        }
                        true
                    }
                    popup.show()
                }
            })

        recycler.adapter = workAdapter

        appStateViewModel.worksLiveData.observe(viewLifecycleOwner) { data ->
            workAdapter.works = data
            workAdapter.notifyDataSetChanged()
        }
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: WorksFragmentArgs by navArgs()
        student = args.owner
        appStateViewModel.getWorks(student.studentId)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.works_menu, menu)

        val addButton = menu.findItem(R.id.add_work)
        addButton.setOnMenuItemClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main)
                .navigate(WorksFragmentDirections.actionToAddWork(student))
            true
        }

        val backButton = menu.findItem(R.id.back)
        backButton.setOnMenuItemClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main).popBackStack()
            true
        }

        super.onCreateOptionsMenu(menu, inflater)
    }
}