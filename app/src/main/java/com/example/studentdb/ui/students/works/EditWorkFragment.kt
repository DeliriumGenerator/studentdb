package com.example.studentdb.ui.students.works

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.studentdb.R
import com.example.studentdb.database.AppStateViewModel
import com.example.studentdb.database.tables.Work
import com.example.studentdb.databinding.FragmentAddWorkBinding

class EditWorkFragment : Fragment() {
    private val appStateViewModel: AppStateViewModel by activityViewModels()
    private var _binding: FragmentAddWorkBinding? = null
    private val binding get() = _binding!!
    private lateinit var work: Work


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddWorkBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: EditWorkFragmentArgs by navArgs()
        work = args.work
        binding.editTextTheme.setText(work.theme)
        binding.editTextSource.setText(work.photo)

        binding.buttonAddPhoto.setOnClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main)
                .navigate(EditWorkFragmentDirections.actionToTakePhoto())
        }

        binding.buttonSave.setOnClickListener {
            val theme = binding.editTextTheme.text.toString()
            val source = binding.editTextSource.text.toString()
            appStateViewModel.editWork(
                    Work(
                        work.workId,
                        work.studentId,
                        theme,
                        source
                    )
                )

            requireActivity().findNavController(R.id.nav_host_fragment_activity_main).popBackStack()
            appStateViewModel.getWorks(work.studentId)
        }

        binding.buttonCancel.setOnClickListener {
            requireActivity().findNavController(R.id.nav_host_fragment_activity_main).popBackStack()
        }
    }
}